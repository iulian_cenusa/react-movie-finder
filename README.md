# Movie Finder App with React

This project was created using [Create React App](https://github.com/facebook/create-react-app), stylized with [Bootstrap 5] (https://getbootstrap.com/docs/5.0/getting-started/introduction/) and data fetched using [TMDB API](https://www.themoviedb.org/documentation/api)

## Images

![Example1](./extras/UI_example1.png)
![Example1](./extras/UI_example2.png)


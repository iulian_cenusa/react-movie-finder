import SearchMovie from './searchMovie';

function App() {
  return (
    <div className="container bg-light ">
      <br />
      <h2 className="d-flex justify-content-center align-items-center"> React Movie Finder </h2> <br />
      <SearchMovie />
    </div>
  );
}

export default App;

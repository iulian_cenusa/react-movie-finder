
export default function MovieCard({movie}) {
    return (
        <>
        <div className="card m-3 p-2" >
            <div className="row">
                <div className="col-4"></div>
                <img className="col-4"
                    src={`https://image.tmdb.org/t/p/w185_and_h278_bestv2/${movie.poster_path}`}
                    alt={movie.title}
                    />
                <div className="col-4"></div>
            </div>
            <div className="row">
                <div className="card-body col">
                    <h3 className="card-title">{movie.title}</h3>
                    <p className="card-text">{movie.overview}</p>
                </div>
            </div>
            <div className="row">
                <div className="col-4">
                    <p className="m-1"> <strong>Release Date:</strong> {movie.release_date}</p>
                </div>
                <div className="col-4">
                    <p className="m-1"><strong>Rating:</strong> {movie.vote_average}</p>
                </div>
                <div className="col-4"></div>
            </div>
        </div>
    
    </>
    )
}

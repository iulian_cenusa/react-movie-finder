import {useState} from 'react';
import MovieCard from './MovieCard';

const { REACT_APP_TMDB_API_KEY } = process.env;

export default function SearchMovie() {

    const [query,setQuery] = useState('');
    const [movies,setMovies] = useState([]);

    const searchMovies = async (e) => {
        e.preventDefault();

        const api_key = REACT_APP_TMDB_API_KEY; 
        
        const url = `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&language=en-US&query=${query}&page=1&include_adult=false`;

        try {
            const res = await fetch(url);
            const data  = await res.json();
            setMovies(data.results);
        }catch(err){
            console.error(err);
        }

    }

    return (
        <>
            <form className="d-flex flex-row m-3" onSubmit={searchMovies}>
                <input className="form-control m-1" type="text" name="query"
                    placeholder="i.e. Jurassic Park" value={query} onChange={(e) => setQuery(e.target.value)} />
                <button className="btn btn-outline-success m-1" type="submit">Find</button>
            </form>
            <div className="d-flex flex-sm-column m-2">
                {movies.filter(movie => movie.poster_path).map(movie => (
                    <MovieCard movie={movie} key={movie.id} />
                ))}
            </div> 
        </>
    )
}
